<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_C2SC_FeedbackScreen</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>b0c0b733-696e-48e8-a91b-868d159e7dfc</testSuiteGuid>
   <testCaseLink>
      <guid>ed5dad0d-6774-49a9-9bb2-a01b53923a27</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_FeedbackScreen/C2SC_Verify_consumer_is_able_to_view_and_enter_the_feedback_in_the_feedback_form-new_user</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>795f6afe-70a0-4690-80e1-7dfa21d84945</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_FeedbackScreen/C2SC_Verify_consumer_is_able_to_view_and_enter_the_feedback_in_the_feedback_form-Registered_user</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>08a86314-e31b-48f4-8228-6328e8a0cc28</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_FeedbackScreen/C2SC_Verify_display_of_success_message_on_successful_submission_of_the_feedback</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
