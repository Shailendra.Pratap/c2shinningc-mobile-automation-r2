<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ForgotEmail1</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>5fa387b6-00de-44a7-b2e4-127ba06dea39</testSuiteGuid>
   <testCaseLink>
      <guid>6fe79872-4ad8-4a9b-97b8-402c478087b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ForgotEmail/C2SC_Verify_the_error_message_for_invalid_code</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>432943a1-8fb8-4f8b-b20b-9c147a54404c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7415fcda-5a20-4f7b-9dde-0085772bb471</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>cd58d648-dfa7-44b4-aa40-7c90dc1b1d23</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ForgotEmail/C2SC_Verify_the_error_message_if_the_invalid_mobile_number_entered</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3fab5147-3b7b-48b0-916e-39a2a7cbc9b9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d25dca0f-2be1-494c-a2a9-61441ff0fbf3</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>4ab848cf-8a6d-4564-ac43-c43468488ae0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ForgotEmail/C2SC_Verify_the_Forgot_email_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e0a5607-ad89-44a9-993f-b82be4ebd601</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ForgotEmail/C2SC_Verify_the_retrieve_email</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15f3a5ff-1b5c-485c-bc9c-74dbdd0c7cd8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ForgotEmail/Verify_the_forgot_password_screen_on_entering_registered_email_id</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
