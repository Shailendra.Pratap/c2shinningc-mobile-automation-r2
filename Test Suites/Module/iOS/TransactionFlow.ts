<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TransactionFlow</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>9b284717-fcd0-474e-9bfd-e689d8edfc91</testSuiteGuid>
   <testCaseLink>
      <guid>ecb5f1aa-4a7d-4a8a-afe6-89e99075777c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TransactionFlow/C2SC_Verify if Application displays a closing screen post cancellation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>642f9070-c64b-44a0-99a1-ed652abba596</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TransactionFlow/C2SC_Verify if feedback options and rating is displayed when clicking cancelled button in closing flow</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5ceec837-dccf-4165-b100-124cde33ce35</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TransactionFlow/C2SC_Verify if text area is displayed to enter comments for cancellation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4f61fbf8-15f2-4d59-8f68-c9ce162a71bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TransactionFlow/C2SC_Verify the Submit button for cancellation after providing mandatory fields</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>845c6bef-2a59-4431-a642-2216eda7ebaa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TransactionFlow/C2SC_Verify the Submit button for cancellation in To do screen before providing mandatory fields</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa9eb97e-2267-4da3-93e0-7cad6077d75f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TransactionFlow/C2SC_Verify_closing_day_contents_on_to_do_screen_can_be_expanded_or_contracted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5615065a-27d1-40fb-af18-6ac1d7d1a090</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TransactionFlow/C2SC_Verify_plus_icon _is_present _and_consumer_task_can_be_added_and _existing_todo_tasks_present_when_closing_flow_triggered</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
