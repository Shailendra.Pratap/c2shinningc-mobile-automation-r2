<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_C2SC_Notifications</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>f81997e3-8dcd-446f-b918-61bd1b824a15</testSuiteGuid>
   <testCaseLink>
      <guid>d48d9158-6e75-4e6e-9bf6-8cb1542285e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Notifications/C2SC_Verify_the_Back_button_in_top_left-corner_in_Notifications_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e6479f3c-f214-4e24-804c-26194021d741</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Notifications/Verify the presence of Clear All option when at least one notification is displayed under notifications section</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>64518edd-ea5c-41ec-b3b1-b0a86152e77c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Notifications/Verify_if_user_is_able_to_view_the_To_do_related_notifications_displayed_on_Notifications_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f42c6d5e-bedf-4455-972d-66ede160438f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Notifications/Verify_the_content_of_notifications_in_notification_tab</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
