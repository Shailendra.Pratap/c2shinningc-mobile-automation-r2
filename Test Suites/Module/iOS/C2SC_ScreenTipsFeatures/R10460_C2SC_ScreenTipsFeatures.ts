<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_C2SC_ScreenTipsFeatures</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>8bdfcd01-04a5-485f-bf45-f9f313178c74</testSuiteGuid>
   <testCaseLink>
      <guid>65390144-3a8b-4047-beee-cb6d204cc5b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ScreenTipsFeatures/C2SC_Verify_consumer_is_able_ to_ view _the_description_when_ he _focuses_ on_ the_feature_Contract</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>84fbb0be-ac6b-4946-a981-bca826896f54</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ScreenTipsFeatures/C2SC_Verify_consumer_is_able_to_view_ the_basic_functionalities_of_the_home_photo_by_clicking_on the_guide_icon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>acbe3bbe-f100-4b6e-996e-0800c2ad4857</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ScreenTipsFeatures/C2SC_Verify_consumer_is_able_to_view_the_ basic_functionalities_ of_the_different_features in _the_app_by_clicking_the_guide_icon_on_the_top</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
