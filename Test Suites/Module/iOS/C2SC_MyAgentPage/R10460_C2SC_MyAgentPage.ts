<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_C2SC_MyAgentPage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>b572af0e-a213-42d5-809f-8a4f4abfedbf</testSuiteGuid>
   <testCaseLink>
      <guid>8a49c22f-9e25-4c38-835f-6c6371ae0abe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_MyAgentPage/C2SC_Verify_buyer_is_redirected_to_the_profile_validation_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5c8ab76b-b1b3-4548-b83b-3f1b2c9532ff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_MyAgentPage/C2SC_Verify_the_Agent_selection_page_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ba325e0f-bec2-45b8-a388-25db3c20dbb8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_MyAgentPage/C2SC_Verify_the_buyer_is_able_to_view_the_My_Agent_page_with_the_selected_agents_contact_details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fdf9a1dc-918c-4827-92df-1dbd93d09025</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_MyAgentPage/C2SC_Verify_the_contact_details_of_the_agent</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2cfc8465-9463-44ea-9224-dd86a7b4eed0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_MyAgentPage/C2SC_Verify_the_Next_option_is_disabled_if_user_not_selecting_an_agent</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5b717eb7-4700-4ee3-ad52-9ed4014a2a0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_MyAgentPage/C2SC_Verify_the_Next_option_is_enabled_on_selecting_the_agent</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
