<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>C2SC_Seller_Homepage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>da582298-1df4-4172-b1b9-cd1a05628a20</testSuiteGuid>
   <testCaseLink>
      <guid>26c43755-3cf7-4ab1-a3a3-5492245a34f9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Seller_Homepage/C2SC_Seller_flow_Verify_the_top_right_corner_of_home_page_to_check_My_purchase_changed_as_My_Listing</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>54ed2695-8dc5-4bb8-a7a8-8689c9b5a1e3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/C2SC_Seller_Homepage/C2SC_Verify_List_price_displayed_in_My_Profile_page_when_sale_price_is_not_updated in_TM</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>79d722d8-6b06-4014-9a84-7e1c09f9fa07</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/C2SC_Seller_Homepage/C2SC_Verify_list_price_is_displayed_in_upper_section_of_homepage_when_closing_date_is_not_updated_in_TM</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>afb08dbf-333c-4f35-bf7f-87dfad674904</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/C2SC_Seller_Homepage/C2SC_Verify_the_upper_section_of_home_page_when_closing_date_is_updated_in_TM_Seller</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5cb6e50a-2535-4de3-b538-fc56d4946982</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/C2SC_Seller_Homepage/C2SC_Verify_timeline_when_closing_date_is_not_updated_in_TM_Seller</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
