<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R12068_C2SC_Address_Map_Details</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>ecaa88a1-1368-4dbf-bd5b-9c58fcb667e5</testSuiteGuid>
   <testCaseLink>
      <guid>f287ebe4-d907-4d70-ae4a-7eb7e993ab0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Address_Map_Details/C2SC_Test_to_check_the_map_option_is_navigating</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3dc575ca-c085-40dc-8a37-3b65ae69813d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Address_Map_Details/C2SC_Test_to_check_the_map_option_is_shown_in_the_My_Profile_details_page</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
