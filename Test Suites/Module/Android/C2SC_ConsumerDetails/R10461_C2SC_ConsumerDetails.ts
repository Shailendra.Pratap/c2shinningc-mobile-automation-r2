<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10461_C2SC_ConsumerDetails</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>ca7cdd61-a191-4c86-bbc5-c9950f6c3884</testSuiteGuid>
   <testCaseLink>
      <guid>2bb5e148-8cbf-417b-8396-363d6e79ab41</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerDetails/C2SC_Test_to_check_the_details_shown_in_profile_validation_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>94ca6b03-c275-4922-b656-dcd67e77b0a8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerDetails/C2SC_Test_to_check_the_Tell_us_about_you_page_after_selecting_the_set_up_my_profile_option</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d245c1c8-ef98-4885-887b-435aa08506a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerDetails/C2SC_Verify_blank_space_is_shown_if_any_of_the_Consumer_details_are_unavailable_on_the_Consumer_details_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3354f4cb-df0c-4cc6-929a-47451ccfd6df</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerDetails/C2SC_Verify_that_buyer_able_to_view_the_details_of_the_property_and_UX_design</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>42c86dbe-3f32-4b04-a142-0fc8c6a8ded5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerDetails/C2SC_Verify_that_buyer_able_to_view_the_Down_payment_of_the_property</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f7cff36-f39b-46bd-8d66-4d68714c206d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerDetails/C2SC_Verify_that_buyer_able_to_view_the_estimated_closing_costs_of_the_property</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c6a1ac78-dafd-4425-bbed-1ae4dce12d30</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerDetails/C2SC_Verify_that_buyer_able_to_view_the_estimated_monthly_cost_of_the_property</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2f134a31-fd21-43ab-b257-de82528576c8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerDetails/C2SC_Verify_that_buyer_able_to_view_the_list_price_of_the_property</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>563ac9f9-55e8-42f4-ba6f-2220ccd884a3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerDetails/C2SC_Verify_that_buyer_able_to_view_the_mortgage_rate_along_with_the_interest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e9b855b8-4c0c-4988-ab9d-d4ef642fbdd0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerDetails/C2SC_Verify_the_Consumer_details_getting_auto_populated_on_the_Consumer_details_screen</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
