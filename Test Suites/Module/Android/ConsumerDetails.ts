<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ConsumerDetails</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>4ef73f3c-4bb2-4111-9f40-b38c9fb2def8</testSuiteGuid>
   <testCaseLink>
      <guid>96a5b7c2-7ce2-4184-9266-aac6b721099c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerDetails/C2SC_Test_to_check_the_details_shown_in_profile_validation_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>64fc4a8c-aa1e-4143-8f69-c1d11d700f59</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerDetails/C2SC_Test_to_check_the_Tell_us_about_you_page_after_selecting_the_set_up_my_profile_option</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>92c10fbb-3e7e-4df5-a19e-a16db51e3c1a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerDetails/C2SC_Verify_blank_space_is_shown_if_any_of_the_Consumer_details_are_unavailable_on_the_Consumer_details_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>49056235-f0c3-42af-957b-d396145ded1b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerDetails/C2SC_Verify_that_buyer_able_to_view_the_details_of_the_property_and_UX_design</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d6d1c561-ff6e-4ceb-a76d-7e38605df206</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerDetails/C2SC_Verify_that_buyer_able_to_view_the_Down_payment_of_the_property</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4d281599-d5d0-4ced-8379-a4c13333ccc2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerDetails/C2SC_Verify_that_buyer_able_to_view_the_estimated_closing_costs_of_the_property</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>08135de4-32cf-4738-878e-4c4125068cf2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerDetails/C2SC_Verify_that_buyer_able_to_view_the_estimated_monthly_cost_of_the_property</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7040b1c3-744e-45a8-b245-fcdc248ff154</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerDetails/C2SC_Verify_that_buyer_able_to_view_the_list_price_of_the_property</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>54f2add9-30d6-43b4-bd09-0b22870f4217</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerDetails/C2SC_Verify_that_buyer_able_to_view_the_mortgage_rate_along_with_the_interest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>41519164-7d74-4e79-ac76-e87d54173647</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerDetails/C2SC_Verify_the_Consumer_details_getting_auto_populated_on_the_Consumer_details_screen</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
