<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10461_C2SC_Homepage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>30a0d111-2110-4051-9f70-c9ac5d526d2d</testSuiteGuid>
   <testCaseLink>
      <guid>973a6227-fb39-4fd2-af84-83c2c7302c57</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_To_verify_display_of_closing_date_and_number_of_days_to_closing</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0e530cde-397e-4785-b7b9-beee370913df</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_To_verify_display_of_Closing_Day_in_place_of_estimated_difference</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1da6fa37-ad9b-4539-8233-ccdd514bea24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_To_verify_display_of_on_track_Icon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5136778b-23d1-423f-b6f2-ccd372fe2781</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_To_verify_Homepage_validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2a401799-acca-4f25-8082-abb7008d6bbd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_To_verify_task_category_contents</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a5f90246-9347-4948-8143-7624bc0bcb13</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify the heading on top of My Docs page got changed from C2C to HomePlace</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>05cf9173-3f11-458f-8642-36308ef11cc3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_consumer_is_able_to_see_Transaction_Completed_screen_once_the_Transaction_is_closed_successfully</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>af3b5b2f-9689-4490-b633-50950fec26a7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f6631b8a-dce9-4670-bdae-ed8c723d77f1</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>1a0f27dd-e4a3-4f3a-bffa-40ed7916c8f9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_consumer_is_able_to_use_all_the_features_as_it_is_after_closing_the_Transaction_successfully</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d70a1be3-cdae-482b-857e-999154ef0604</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5549cdd0-3638-4fda-bdc4-995227772741</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>612a1fdf-ccc0-4db2-bd27-2d47686f15d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_Mark as_done_under_submit_your_initial_earnest_money</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>540b5c69-4143-43ae-9f0b-9855f147719c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_Mark_as_done_in_the_to_do_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cff5eb5c-87f3-41e7-883b-e455af6db143</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_that_no_document_is_available_for_the_to_do_task_when_a_no_service_provider_is_selected</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fbbe3dee-86a2-4dd4-9af3-b84dce7ad116</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_that_no_document_is_available_for_the_to_do_task_when_a_recommended_service_provider_is_selected</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9650e56a-6e13-4ae4-9faa-769451e81be9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_that_no_document_is_available_for_the_to_do_task_when_a_self_preferred_service_provider_is_selected</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>da9e85a8-ec1d-4339-b0d9-e0eab0fbcf34</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_that_title_Schedule_Open_House_is_displayed_in_To_Do_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>838692cb-d23d-4cfa-aef1-c8c92f885083</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_appearance_of_obtain_homeowners_insurance_and_closing_documents_in_to_do_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c1953b75-532d-4618-be62-338c275f1fd3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_appearance_of_Submit_your_initial_earnest_money</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7cf02fb9-6e0a-49e7-9e42-d4c03d4abfce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_close_icon_X_in_schedule_inspection_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c6bad7ee-74ac-4cd4-9b49-d129fc60ad58</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Date_field_in_the_form</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a00c6277-5b52-4b52-a328-b5e72304f5a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_error_message_displayed_for_date_field</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6988e715-d54c-44d2-b0c7-22f81ee17ed0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_error_message_displayed_for_End_Time_field</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8453248a-cab1-4575-82e7-616fa090c679</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_error_message_displayed_for_Start_Time_field</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1b8f0229-52b3-4089-88de-3b61138c72fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_error_message_for_Time_field in_schedule_inspection_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b9d92daa-cbe4-43ad-a37e-5a4b4e5c78a4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_fields_shown_for_closing_documents_task_in_to_do</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a1fbec79-f36b-4f42-abfa-942a6586fcc2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_fields_shown_for_obtain_homeowners_insurance_task_in_to_do</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>47c04923-b5a6-4dc8-8e83-65615f4a8728</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_fields_shown_for_Title_commitment_form_task_in_to_do</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8c4860a5-eae6-4d1d-a2f0-b325aef9c713</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_heading_on_top_of_Home_page_got_changed_from_C2C_to_HomePlace</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ddb88cf5-c447-4366-8d52-954005d05807</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_heading_on_top_of_My_Agent_page_got_changed_from_C2C_to_HomePlace</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d27d807c-7699-47ea-a9e1-e75bd5e07614</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_heading_on_top_of_My_Profile_page_got_changed_from_C2C_to_HomePlace</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2bea08bc-485b-4522-9d08-a62c75da57ba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_heading_on_top_of_My_Team_page_got_changed_from_C2C_to_HomePlace</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e0c2421-5bdb-4293-a207-50e85242ab54</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Inspection_Company_field_in_the_form</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>810735c8-b696-4f29-ac18-5b5664de4f8d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_options_under_submit_your_initial_earnest_money</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>34ed414d-b484-447e-b35a-500be6b1166d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Schedule_Inspection_on_clicking_of_done</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>df77d6d2-cf7c-4c0b-8ad8-072f6048a1af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_sequence_in_Todo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>151ac397-037c-4ee4-a15b-22889cb28eb4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_verify_the_status_on_click_of_Obtain_Homeowners_insurance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>60e162d2-d871-47ec-94cb-9c00eca721da</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Time_field_in_the_form</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>04e451e6-2f49-4e44-a2e1-b926fe46e06e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Title_description_due_days_for_Submit_your_initial_earnest_money_task_in_todo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0caf4926-914c-4f92-8c2e-27e36a630d0f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_to-do_page_and_timeline_after_adding_Schedule_Open_House_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c6fec4d-2dec-4dbc-b98e-ca874e0a0ebb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Wire_Funds_for_closing_pdf_task_in_to_do</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
