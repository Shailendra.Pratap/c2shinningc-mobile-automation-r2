package dataBaseConnection

import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.*

import com.kms.katalon.core.annotation.Keyword


public class DBConnect {

	private static Connection connection = null;

	/**
	 * Open and return a connection to database
	 * @param dataFile absolute file path
	 * @return an instance of java.sql.Connection
	 */

	//def connectDB(String dataFile){
	@Keyword
	def connectDB(){
		//private static Connection connection = null;
		//Load driver class for your specific database type
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver")
		String connectionString = "jdbc:sqlserver://rfgstg5:1435;databaseName=DASH;user=rfglistingservice;password=test"


		if(connection != null && !connection.isClosed()){
			connection.close()
		}
		connection = DriverManager.getConnection(connectionString)

		return connection
	}


	/**
	 * execute a SQL query on database
	 * @param queryString SQL query string
	 * @return a reference to returned data collection, an instance of java.sql.ResultSet
	 */
	@Keyword
	def executeQuery(String sQueryString) {
		Statement stm = connection.createStatement()
		ResultSet rs = stm.executeQuery(sQueryString)
		return rs
	}

	@Keyword
	def closeDatabaseConnection() {
		if(connection != null && !connection.isClosed()){
			connection.close()
		}
		connection = null
	}

	/**
	 * Execute non-query (usually INSERT/UPDATE/DELETE/COUNT/SUM...) on database
	 * @param queryString a SQL statement
	 * @return single value result of SQL statement
	 */
	@Keyword
	def execute(String queryString) {
		Statement stm = connection.createStatement()
		boolean result = stm.execute(queryString)
		return result
	}

	@Keyword

	def getValue(ResultSet resultset , String sValueReqd)
	{
		resultset.getString(sValueReqd)
	}
}
