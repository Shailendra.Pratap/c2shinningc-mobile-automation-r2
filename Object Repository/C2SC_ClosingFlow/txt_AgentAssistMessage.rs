<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>txt_AgentAssistMessage</name>
   <tag></tag>
   <elementGuidId>feb4b432-e981-48e3-a84a-68156d50d0e8</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>XCUIElementTypeStaticText</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>agent_assist_lbl</value>
   </webElementProperties>
   <locator>//*[@type = 'XCUIElementTypeStaticText' and @name = 'agent_assist_lbl' and @label = 'agent_assist_lbl']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
