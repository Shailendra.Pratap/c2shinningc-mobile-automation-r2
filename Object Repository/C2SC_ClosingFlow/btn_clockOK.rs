<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>btn_clockOK</name>
   <tag></tag>
   <elementGuidId>b5b3ed1b-7079-454f-a83b-c67fef5e2154</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>OK</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>android.widget.Button</value>
   </webElementProperties>
   <locator>//*[(@text = 'OK' or . = 'OK') and @class = 'android.widget.Button']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
