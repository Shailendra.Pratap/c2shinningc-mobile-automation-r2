<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>txt_Timeline_TranscationReschedule</name>
   <tag></tag>
   <elementGuidId>d2dc49a9-314a-43d9-ae15-834bb2ebde0e</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Transaction Rescheduled</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>	com.realogy.C2CShining:id/task_title</value>
   </webElementProperties>
   <locator>//*[(@text = 'Transaction Rescheduled' or . = 'Transaction Rescheduled') and @resource-id = '	com.realogy.C2CShining:id/task_title']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
