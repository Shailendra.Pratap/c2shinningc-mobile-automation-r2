<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>txt_Time</name>
   <tag></tag>
   <elementGuidId>40bf1362-ea3d-492d-b9ba-1e1e0589506b</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>XCUIElementTypeOther</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Schedule_txt_time</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>	com.realogy.C2CShining:id/edittext_time</value>
   </webElementProperties>
   <locator>//*[@type = 'XCUIElementTypeOther' and @name = 'Schedule_txt_time'] | //*[@resource-id = 'com.realogy.C2CShining:id/edittext_time']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
