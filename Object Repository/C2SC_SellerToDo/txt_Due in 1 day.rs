<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>txt_Due in 1 day</name>
   <tag></tag>
   <elementGuidId>c5f03b29-d8c5-40fe-b665-b27b677beaa1</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Due in 1 day</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>	com.realogy.C2CShining:id/tvToDoDueDate</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>android.widget.TextView</value>
   </webElementProperties>
   <locator>//*[(@text = 'Due in 1 day' or . = 'Due in 1 day') and @resource-id = '	com.realogy.C2CShining:id/tvToDoDueDate' and @class = 'android.widget.TextView']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
