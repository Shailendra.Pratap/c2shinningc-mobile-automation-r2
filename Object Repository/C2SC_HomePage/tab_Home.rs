<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>tab_Home</name>
   <tag></tag>
   <elementGuidId>0a9623b0-d2d5-4a42-8c13-13e519b8a7d3</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>XCUIElementTypeButton</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Home</value>
   </webElementProperties>
   <locator>//*[@type = 'XCUIElementTypeButton' and @name = 'Home'] | //*[contains(@text,'Home')]</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
