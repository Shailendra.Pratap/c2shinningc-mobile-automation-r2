<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>img_EyeConfirmPassword</name>
   <tag></tag>
   <elementGuidId>11df9392-f5f2-45e0-aaec-4bf11b368946</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>	com.realogy.C2CShining:id/eye_icon_confirm_password</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>android.widget.ImageView</value>
   </webElementProperties>
   <locator>//*[@class='android.widget.ImageView' and @resource-id='com.realogy.C2CShining:id/eye_icon_confirm_password']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
