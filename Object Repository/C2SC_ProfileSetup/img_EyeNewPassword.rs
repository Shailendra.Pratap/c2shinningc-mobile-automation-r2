<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>img_EyeNewPassword</name>
   <tag></tag>
   <elementGuidId>9bc3465f-6c2a-4d72-9656-3638ace75c88</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>	com.realogy.C2CShining:id/eye_icon_new_password</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>android.widget.ImageView</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>content-desc</name>
      <type>Main</type>
      <value>change password view</value>
   </webElementProperties>
   <locator>//*[@class='android.widget.ImageView' and @resource-id='com.realogy.C2CShining:id/eye_icon_new_password']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
