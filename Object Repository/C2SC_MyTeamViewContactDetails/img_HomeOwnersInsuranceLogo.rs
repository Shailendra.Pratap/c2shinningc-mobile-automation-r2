<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>img_HomeOwnersInsuranceLogo</name>
   <tag></tag>
   <elementGuidId>9a17994d-fe01-4b5b-bf9d-b765539eda7d</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>android.widget.ImageView</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>com.realogy.C2CShining:id/iv_service_type</value>
   </webElementProperties>
   <locator>//*[@class='android.widget.ImageView' and @resource-id='com.realogy.C2CShining:id/iv_service_type']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
