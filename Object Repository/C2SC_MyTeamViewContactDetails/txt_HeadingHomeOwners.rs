<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>txt_HeadingHomeOwners</name>
   <tag></tag>
   <elementGuidId>3ae912ff-4575-4710-b355-c92142148eb5</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <locator>//*[@class='android.widget.TextView' and @text='Home Owners Insurance' and @resource-id='com.realogy.C2CShining:id/tv_category']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
