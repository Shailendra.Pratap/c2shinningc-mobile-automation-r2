<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>txt_ScheduleWalkthrough</name>
   <tag></tag>
   <elementGuidId>aeb3e467-628b-47d3-9cf9-0b1465896876</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>XCUIElementTypeStaticText</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Schedule Walkthrough</value>
   </webElementProperties>
   <locator>//*[@type = 'XCUIElementTypeStaticText' and @name = 'Schedule Walkthrough'] | //*[@text = 'Schedule Walkthrough' and @resource-id = 'com.realogy.C2CShining:id/my_journey_task_title']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
