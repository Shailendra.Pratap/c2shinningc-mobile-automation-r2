<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>txt_ScheduleWalkthroughDescription</name>
   <tag></tag>
   <elementGuidId>ecf5f449-099f-4876-9f9f-a11469ff3441</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>XCUIElementTypeStaticText</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>The buyer will do a final walkthrough prior to closing. Let your agent know when you will be out of the property.</value>
   </webElementProperties>
   <locator>//*[@type = 'XCUIElementTypeStaticText' and @name = 'The buyer will do a final walkthrough prior to closing. Let your agent know when you will be out of the property.'] | //*[contains(@text,'The buyer will do a final walkthrough prior to closing. Let your agent know when you will be out of the property.)']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
