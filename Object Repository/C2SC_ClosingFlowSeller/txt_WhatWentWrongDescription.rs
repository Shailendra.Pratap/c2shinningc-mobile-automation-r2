<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>txt_WhatWentWrongDescription</name>
   <tag></tag>
   <elementGuidId>a26db662-77fa-4f2e-b80d-60354ee9cc0c</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>	com.realogy.C2CShining:id/what_went_wrong_desc_text</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>	We are sorry your purchase didn't close successfully, but remember your agent is always here to assist you with your Real Estate needs.</value>
   </webElementProperties>
   <locator>//*[@resource-id = '	com.realogy.C2CShining:id/what_went_wrong_desc_text' and (@text = concat('	We are sorry your purchase didn' , &quot;'&quot; , 't close successfully, but remember your agent is always here to assist you with your Real Estate needs.') or . = concat('	We are sorry your purchase didn' , &quot;'&quot; , 't close successfully, but remember your agent is always here to assist you with your Real Estate needs.'))]</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
