<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>btn_Cancelled</name>
   <tag></tag>
   <elementGuidId>f771d90b-4932-41a2-9bd2-8c382c162255</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>com.realogy.C2CShining:id/btn_cancelled</value>
   </webElementProperties>
   <locator>//*[@resource-id = 'com.realogy.C2CShining:id/btn_cancelled']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
