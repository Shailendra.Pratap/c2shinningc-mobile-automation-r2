<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>btn_Add</name>
   <tag></tag>
   <elementGuidId>817129ed-023c-4d29-8f3f-dbe2b2ff9276</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>com.realogy.C2CShining:id/schedule_btn</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add</value>
   </webElementProperties>
   <locator>//*[@resource-id = 'com.realogy.C2CShining:id/schedule_btn' and (@text = 'Add' or . = 'Add')]</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
