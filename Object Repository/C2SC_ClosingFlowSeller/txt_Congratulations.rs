<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>txt_Congratulations</name>
   <tag></tag>
   <elementGuidId>beef896d-91de-4385-b663-efd3e9ff6749</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>com.realogy.C2CShining:id/congratulations_text</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>android.widget.TextView</value>
   </webElementProperties>
   <locator>//*[@resource-id = 'com.realogy.C2CShining:id/congratulations_text' and @class = 'android.widget.TextView']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
