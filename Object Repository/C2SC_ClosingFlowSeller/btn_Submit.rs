<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>btn_Submit</name>
   <tag></tag>
   <elementGuidId>8544ee84-3646-45a8-b82c-c882175cdbb7</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>com.realogy.C2CShining:id/submit_button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>android.widget.Button</value>
   </webElementProperties>
   <locator>//*[@resource-id = 'com.realogy.C2CShining:id/submit_button' and @class = 'android.widget.Button']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
