<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>btn_SomethingWentWrong</name>
   <tag></tag>
   <elementGuidId>0ecd725c-69ad-4fe1-9dca-b6514a588c9f</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>	com.realogy.C2CShining:id/something_went_wrong_button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>android.widget.ToggleButton</value>
   </webElementProperties>
   <locator>//*[@resource-id = '	com.realogy.C2CShining:id/something_went_wrong_button' and @class = 'android.widget.ToggleButton']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
