<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>txt_RememberMe</name>
   <tag></tag>
   <elementGuidId>b27afbe9-6702-4420-a3b0-07dadf2c795b</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>XCUIElementTypeStaticText</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Remember Me</value>
   </webElementProperties>
   <locator>//*[@type = 'XCUIElementTypeStaticText' and @name = 'Remember Me'] | //*[@text = 'Remember Me' and @class = 'android.widget.CheckBox' and @resource-id = 'com.realogy.C2CShining:id/saveLoginCheckBox']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
