import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_Login'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('C2SC_ProfileSetup/btn_Agent'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_Agent'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('C2SC_ProfileSetup/btn_Next'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_Next'), GlobalVariable.intWaitTime)

//Mobile.verifyElementExist(findTestObject('C2SC_ConsumerDetailsScreen/txt_WelcomeText'), GlobalVariable.intWaitTime)
not_run: Mobile.waitForElementPresent(findTestObject('C2SC_ConsumerDetailsScreen/txt_ValidateInfo'), GlobalVariable.intWaitTime)

not_run: Mobile.getText(findTestObject('C2SC_ConsumerDetailsScreen/txt_ValidateInfo'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_ProfilePage/txt_FirstName'), GlobalVariable.intWaitTime)

//Mobile.verifyElementText(findTestObject('ConsumerDetailsScreen/input_firstnamefield'),  '')
Mobile.verifyElementExist(findTestObject('C2SC_ProfilePage/txt_LastName'), GlobalVariable.intWaitTime)

//Mobile.verifyElementText(findTestObject('ConsumerDetailsScreen/input_lastNameField'), '')
Mobile.verifyElementExist(findTestObject('C2SC_ProfilePage/txt_Email'), GlobalVariable.intWaitTime)

Mobile.scrollToText('Set up Profile')

Mobile.verifyElementExist(findTestObject('C2SC_ProfilePage/txt_Phone'), GlobalVariable.intWaitTime)

//Mobile.verifyElementText(findTestObject('ConsumerDetailsScreen/input_phoneField'),  '')
Mobile.closeApplication()

