import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.apache.commons.lang.RandomStringUtils as RandomStringUtils

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_HomePageFlow'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.delay(10)

Mobile.waitForElementPresent(findTestObject('C2SC_MyProfilePage/tab_MyProfile'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ChangePasswordFromMyProfileScreen/tab_MyProfile'), GlobalVariable.intWaitTime)

Mobile.scrollToText('Logout')

Mobile.verifyElementExist(findTestObject('C2SC_ChangePasswordFromMyProfileScreen/btn_changePassword'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ChangePasswordFromMyProfileScreen/btn_changePassword'), GlobalVariable.intWaitTime)

Mobile.verifyElementText(findTestObject('C2SC_ChangePasswordFromMyProfileScreen/txt_changePassword'), 'Change Password')

Mobile.tapAndHold(findTestObject('C2SC_ChangePasswordFromMyProfileScreen/txt_CurrentPassword'), GlobalVariable.intWaitTime, 
    0)

Mobile.setText(findTestObject('C2SC_ChangePasswordFromMyProfileScreen/txt_CurrentPassword'), 'Mindtree@1234', GlobalVariable.intWaitTime)

Mobile.hideKeyboard()

Mobile.tapAndHold(findTestObject('C2SC_ChangePasswordFromMyProfileScreen/txt_NewPassword'), GlobalVariable.intWaitTime, 
    0)

Mobile.setText(findTestObject('C2SC_ChangePasswordFromMyProfileScreen/txt_NewPassword'), 'Welcome123$', GlobalVariable.intWaitTime)

Mobile.hideKeyboard()

Mobile.tapAndHold(findTestObject('C2SC_ChangePasswordFromMyProfileScreen/txt_ConfirmPassword'), GlobalVariable.intWaitTime, 
    0)

Mobile.setText(findTestObject('C2SC_ChangePasswordFromMyProfileScreen/txt_ConfirmPassword'), 'Welcome123$', GlobalVariable.intWaitTime)

Mobile.hideKeyboard()

Mobile.delay(3)

Mobile.tap(findTestObject('C2SC_ChangePasswordFromMyProfileScreen/btn_continue'), GlobalVariable.intWaitTime)

Mobile.delay(3)

Mobile.verifyElementExist(findTestObject('C2SC_ChangePasswordFromMyProfileScreen/img_coldwell'), GlobalVariable.intWaitTime)

Mobile.closeApplication()

