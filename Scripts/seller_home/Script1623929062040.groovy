import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('bs://3e584e6cd6b74d04fcf539375d7c3215fa060766', true)

Mobile.tap(findTestObject('null'), 0)

Mobile.tapAndHold(findTestObject('null'), 0, 0)

Mobile.tapAndHold(findTestObject('null'), 0, 0)

Mobile.setText(findTestObject(''), 'seltest18@yopmail.com', 0)

Mobile.tapAndHold(findTestObject('null'), 0, 0)

Mobile.setText(findTestObject('null'), 'Welcome123', 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('null'), 0)

Mobile.getText(findTestObject('seller_home/txt_ListPrice'), 0)

Mobile.tap(findTestObject('Object Repository/seller_home/android.widget.TextView'), 0)

Mobile.getText(findTestObject('Object Repository/seller_home/android.widget.TextView - List Price'), 0)

Mobile.getText(findTestObject('seller_home/txt_Insights'), 0)

Mobile.tap(findTestObject('Object Repository/seller_home/android.widget.ImageView'), 0)

Mobile.closeApplication()

