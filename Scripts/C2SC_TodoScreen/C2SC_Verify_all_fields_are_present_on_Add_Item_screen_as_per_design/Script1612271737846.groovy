import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_EmailLogin'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('C2SC_AddToDoItemScreen/tab_ToDo'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/tab_ToDo'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_AddToDoItemScreen/btn_plusIcon'), GlobalVariable.intWaitTime, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/btn_plusIcon'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_AddToDoItemScreen/txt_AddItemToYourList'), GlobalVariable.intWaitTime)

def title = Mobile.getText(findTestObject('C2SC_AddToDoItemScreen/txt_Title'), GlobalVariable.intWaitTime)

Mobile.verifyEqual(title.toString().replaceAll('\\s', ''), 'Title')

def description = Mobile.getText(findTestObject('C2SC_AddToDoItemScreen/txt_Description'), GlobalVariable.intWaitTime)

Mobile.verifyEqual(description.toString().replaceAll('\\s', ''), 'Description')

def dueDate = Mobile.getText(findTestObject('C2SC_AddToDoItemScreen/txt_DueDate'), GlobalVariable.intWaitTime)

Mobile.verifyEqual(dueDate.toString().trim(), 'Due Date')

Mobile.getText(findTestObject('C2SC_AddToDoItemScreen/txt_Priority'), GlobalVariable.intWaitTime)

Mobile.getText(findTestObject('C2SC_AddToDoItemScreen/lnk_AddAnotherItem'), GlobalVariable.intWaitTime)

Mobile.getText(findTestObject('C2SC_AddToDoItemScreen/btn_Save'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_AddToDoItemScreen/img_closeIcon'), GlobalVariable.intWaitTime)

Mobile.closeApplication()

