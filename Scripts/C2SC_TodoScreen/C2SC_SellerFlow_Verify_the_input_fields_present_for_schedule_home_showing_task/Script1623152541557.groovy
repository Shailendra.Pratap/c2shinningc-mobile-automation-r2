import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_EmailLogin_Seller'), [:], FailureHandling.STOP_ON_FAILURE)

if(GlobalVariable.sDeviceAndroid.equals('iOS'))
	{
		try {
			Mobile.waitForElementPresent(findTestObject('Object Repository/C2SC_HomePage/txt_ScheduleHomeShowing'), GlobalVariable.intWaitTime)
			Mobile.tap(findTestObject('Object Repository/C2SC_HomePage/txt_ScheduleHomeShowing'), GlobalVariable.intWaitTime)
			}catch(Exception e) {
				
				Mobile.swipe(16,515,16,190)
				Mobile.waitForElementPresent(findTestObject('Object Repository/C2SC_HomePage/txt_ScheduleHomeShowing'), GlobalVariable.intWaitTime)
				Mobile.tap(findTestObject('Object Repository/C2SC_HomePage/txt_ScheduleHomeShowing'), GlobalVariable.intWaitTime)
				}
	}else {
	Mobile.scrollToText("Schedule Home Showing")

Mobile.waitForElementPresent(findTestObject('Object Repository/C2SC_HomePage/txt_ScheduleHomeShowing'), GlobalVariable.intWaitTime)
Mobile.tap(findTestObject('Object Repository/C2SC_HomePage/txt_ScheduleHomeShowing'), GlobalVariable.intWaitTime)
	}
Mobile.waitForElementPresent(findTestObject('Object Repository/C2SC_AddToDoItemScreen/title_ScheduleOpenHouse'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_AddToDoItemScreen/txt_DueDate'), GlobalVariable.intWaitTime)
Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_AddToDoItemScreen/input_StartsAt'), GlobalVariable.intWaitTime)
Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_AddToDoItemScreen/input_EndsAt'), GlobalVariable.intWaitTime)

Mobile.closeApplication()