import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('bs://2fa2a19284598f5067a0d0e4a504551a0fea28e4', true)

Mobile.tap(findTestObject('Object Repository/sample/android.widget.TextView - Log In'), 0)

Mobile.tapAndHold(findTestObject('Object Repository/sample/android.widget.EditText'), 0, 0)

Mobile.setText(findTestObject('Object Repository/sample/android.widget.EditText (1)'), 'c2sc9703@test.com', 0)

Mobile.tapAndHold(findTestObject('Object Repository/sample/android.widget.EditText (2)'), 0, 0)

Mobile.setText(findTestObject('Object Repository/sample/android.widget.EditText (3)'), 'Mindtree@1234', 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Object Repository/sample/android.widget.Button - Submit'), 0)

Mobile.scrollToText('Schedule your Closing')

Mobile.scrollToText('Schedule Walkthrough')

Mobile.getText(findTestObject('sample/txt_ScheduleWalkthrough'), 0)

Mobile.getText(findTestObject('sample/txt_ScheduleWalkthroughDesc'), 
    0)

Mobile.tap(findTestObject(''), 0)

Mobile.getText(findTestObject('sample/txt_DueDate'), 0)

Mobile.getText(findTestObject('sample/txt_Starts at'), 0)

Mobile.getText(findTestObject('sample/txt_Endsat'), 0)

Mobile.tap(findTestObject('Object Repository/sample/android.widget.EditText (4)'), 0)

Mobile.tap(findTestObject('Object Repository/sample/android.widget.Button - OK'), 0)

Mobile.setText(findTestObject('Object Repository/sample/android.widget.EditText (5)'), 'test', 0)

Mobile.tap(findTestObject('Object Repository/sample/android.widget.EditText (6)'), 0)

Mobile.tap(findTestObject('Object Repository/sample/android.widget.Button - OK (1)'), 0)

Mobile.tap(findTestObject('Object Repository/sample/android.widget.Button - Close app'), 0)

Mobile.tap(findTestObject('Object Repository/sample/android.view.View'), 0)

Mobile.tap(findTestObject('Object Repository/sample/android.view.View (1)'), 0)

Mobile.closeApplication()

