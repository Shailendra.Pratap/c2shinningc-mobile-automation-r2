import com.kms.katalon.core.mobile.keyword.internal.MobileAbstractKeyword

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.JavascriptExecutor
import io.appium.java_client.AppiumDriver
import org.openqa.selenium.JavascriptExecutor
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import io.appium.java_client.ios.IOSDriver
import org.openqa.selenium.interactions.Actions
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_EmailLogin_Seller'), [:], FailureHandling.STOP_ON_FAILURE)
Mobile.delay(GlobalVariable.intWaitTime, FailureHandling.STOP_ON_FAILURE)

if (Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_GraSsnParameter/btn_OK'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)) {
	Mobile.tap(findTestObject('Object Repository/C2SC_GraSsnParameter/btn_OK'), GlobalVariable.intWaitTime)
    Mobile.tap(findTestObject('Object Repository/C2SC_GraSsnParameter/btn_OK'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)
}

String AndroidDevice = GlobalVariable.sDeviceAndroid


if (AndroidDevice.equals('Android')) {

	Mobile.scrollToText("Schedule your Closing")
	
	} else {
		
		Mobile.swipe(170, 640, 170, 490)
		Mobile.delay(3)
		Mobile.swipe(170, 640, 170, 490)
		Mobile.delay(3)
		Mobile.swipe(170, 640, 170, 490)
		Mobile.delay(3)
		Mobile.swipe(170, 640, 170, 490)
		Mobile.delay(3)
	}

Mobile.tap(findTestObject('C2SC_ScreenTips/txt_Schedule your Closing'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_HomePage/img_HelpIcon'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('C2SC_ScreenTips/txt_scheduleClosingScreenTip'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ScreenTips/txt_Close'), GlobalVariable.intWaitTime)

Mobile.closeApplication()