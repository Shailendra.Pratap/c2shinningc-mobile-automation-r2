import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_EmailLogin'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('C2SC_HomePage/txt_My Team'), GlobalVariable.intWaitTime)

def validateMortgage = Mobile.getText(findTestObject('C2SC_MyTeamPage/txt_Mortgage'), GlobalVariable.intWaitTime)

Mobile.verifyEqual(validateMortgage, 'Mortgage')

Mobile.tap(findTestObject('C2SC_MyTeamPage/txt_Mortgage'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_MyTeamPage/txt_Accept'), GlobalVariable.intWaitTime)

//Mobile.tap(findTestObject('Object Repository/C2SC_MyTeamViewContactDetails/input_ssn'),GlobalVariable.intWaitTime)
//
//Mobile.setText(findTestObject('Object Repository/C2SC_MyTeamViewContactDetails/input_ssn'), '1236', GlobalVariable.intWaitTime)
//
//String AndroidDevice = GlobalVariable.sDeviceAndroid
//if (AndroidDevice.equals('iOS')) {
//	Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
//}
//
//Mobile.tap(findTestObject('Object Repository/C2SC_MyTeamPage/btn_ssn_btn_check'), GlobalVariable.intWaitTime)
//
//
//Mobile.tap(findTestObject('Object Repository/C2SC_MyTeamViewContactDetails/btn_Save'), GlobalVariable.intWaitTime)
//
////Mobile.tap(findTestObject('Object Repository/C2SC_MyTeamViewContactDetails/Error OK'), 0)
////Mobile.tap(findTestObject('Object Repository/C2SC_MyTeamViewContactDetails/android.widget.ImageView (9)'), 0)
////Mobile.getText(findTestObject('Object Repository/C2SC_MyTeamViewContactDetails/img_gra'), 0)
//Mobile.verifyElementExist(findTestObject('C2SC_MyTeamPage/img_Guaranteed'), GlobalVariable.intWaitTime)
//
//Mobile.tap(findTestObject('C2SC_MyTeamPage/img_Guaranteed'), GlobalVariable.intWaitTime)
//
