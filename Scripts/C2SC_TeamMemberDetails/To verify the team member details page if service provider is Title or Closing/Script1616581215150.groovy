import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_EmailLogin'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('C2SC_HomePage/txt_My Team'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('C2SC_HomePage/txt_My Team'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_HomePage/txt_My Team'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('Object Repository/C2SC_MyTeamPage/txt_TitleClosing'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_MyTeamPage/txt_TitleClosing'), GlobalVariable.intWaitTime)

//If the Title/closing is from TM/Customer already selected recommended service provider
if(Mobile.verifyElementVisible(findTestObject('C2SC_MyTeamPage/btn_Accept'),GlobalVariable.intWaitTime,FailureHandling.OPTIONAL)){
	
	Mobile.tap(findTestObject('C2SC_MyTeamPage/btn_Accept'),GlobalVariable.intWaitTime)
	
	Mobile.delay(GlobalVariable.intWaitTime);
	
	Mobile.waitForElementPresent(findTestObject('Object Repository/C2SC_MyTeamPage/img_verifyTitle'), GlobalVariable.intWaitTime)
	
	Mobile.tap(findTestObject('Object Repository/C2SC_MyTeamPage/txt_TitleClosing'), GlobalVariable.intWaitTime)
}

//Mobile.tap(findTestObject('Object Repository/C2SC_MyTeamViewContactDetails/img_TitleClosing'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_MyTeamPage/input_ContactName'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_MyTeamPage/input_Email'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_MyTeamPage/input_PhoneNumber'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_MyTeamViewContactDetails/txt_Web Link'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_MyTeamPage/btn_Delete'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_MyTeamViewContactDetails/img_SunbeltLogo'), GlobalVariable.intWaitTime)

//Mobile.verifyElementNotChecked(findTestObject('Object Repository/C2SC_MyTeamViewContactDetails/btn_Edit'), GlobalVariable.intWaitTime)
//
//Mobile.verifyElementExist(findTestObject('C2SC_MyTeamViewContactDetails/txt_HeadingTitleClosing'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_MyTeamPage/lnk_X'), GlobalVariable.intWaitTime)

//Mobile.verifyElementExist(findTestObject('C2SC_HomePage/txt_My Team'), GlobalVariable.intWaitTime)

Mobile.closeApplication()

