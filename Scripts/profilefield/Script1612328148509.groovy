import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('bs://7a6e136fe91ea25da730f18ebaa677f37712dd5f', true)

Mobile.tap(findTestObject(''), 0)

Mobile.setText(findTestObject(''), '1234567890', 0)

Mobile.setText(findTestObject(''), '1', 0)

Mobile.setText(findTestObject(''), '2', 0)

Mobile.setText(findTestObject(''), '3', 0)

Mobile.setText(findTestObject(''), '4', 0)

Mobile.tap(findTestObject('Object Repository/Latest_iOS_Object/XCUIElementTypeTextField (5)'), 0)

Mobile.setText(findTestObject(''), 'test', 0)

Mobile.tap(findTestObject('Object Repository/Latest_iOS_Object/XCUIElementTypeTextField (6)'), 0)

Mobile.setText(findTestObject(''), 'test', 0)

Mobile.tap(findTestObject('Object Repository/Latest_iOS_Object/XCUIElementTypeTextField (7)'), 0)

Mobile.setText(findTestObject(''), 'test@gmail.com', 0)

Mobile.tap(findTestObject('Object Repository/Latest_iOS_Object/XCUIElementTypeTextField (8)'), 0)

Mobile.setText(findTestObject(''), '1234567890', 0)

Mobile.closeApplication()

