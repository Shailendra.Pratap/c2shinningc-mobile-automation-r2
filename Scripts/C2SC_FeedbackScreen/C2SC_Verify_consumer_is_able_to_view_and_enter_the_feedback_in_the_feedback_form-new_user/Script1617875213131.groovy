import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_HomePageFlow'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('C2SC_MyProfilePage/tab_MyProfile'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_FeedbackScreen/txt_Feedback'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_FeedbackScreen/txt_Feedback'), GlobalVariable.intWaitTime)

Mobile.verifyElementText(findTestObject('Object Repository/C2SC_FeedbackScreen/txt_FeedbackHeader'), 'Feedback')

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_FeedbackScreen/txt_FeedbackSubtitle'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_FeedbackScreen/txt_Description'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_FeedbackScreen/Btn_Submit'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_FeedbackScreen/Btn_Submit'), GlobalVariable.intWaitTime)

device = GlobalVariable.sDeviceAndroid

if(device == 'Android') {
	Mobile.verifyElementText(findTestObject('Object Repository/C2SC_FeedbackScreen/txt_EnterFeedbackValidation'), 'Please enter a feedback')
}
else {
Mobile.verifyElementText(findTestObject('Object Repository/C2SC_FeedbackScreen/txt_EnterFeedbackValidation'), 'Please enter a valid value')
}
Mobile.setText(findTestObject('Object Repository/C2SC_FeedbackScreen/Input_Description'), 'This is test description', GlobalVariable.intWaitTime)

if(Mobile.verifyElementExist(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)) {
	Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
}

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_FeedbackScreen/Btn_Submit'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_FeedbackScreen/Btn_Submit'), GlobalVariable.intWaitTime)

Mobile.closeApplication()