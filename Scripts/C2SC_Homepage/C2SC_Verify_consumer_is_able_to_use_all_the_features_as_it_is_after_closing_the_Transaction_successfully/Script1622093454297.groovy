import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('C2SC_Homepage/C2SC_Verify_consumer_is_able_to_see_Transaction_Completed_screen_once_the_Transaction_is_closed_successfully'), 
    [('lstReturned') : [:], ('requestBodyPOST') : '{\r\n    "sourceApp": "MyDeals",\r\n    "transactionId": "9899111122",\r\n    "requestTime": "2021-05-20",\r\n    "officeId": "99007839",\r\n    "estimatedClosingDate": "2021-05-25",\r\n    "side": "sell",\r\n    "contractDate": "2021-05-20",\r\n    "listingAgreementDate": "2021-05-20",\r\n    "agent": [\r\n        {\r\n            "agentOktaId": "00urwnkh6p6TfkovG0h7",\r\n            "agentTridentId": "TestAgent2OktaId"\r\n        }\r\n    ],\r\n    "property": {\r\n        "mlsID": "123",\r\n        "addressLine1": "street1",\r\n        "addressLine2": "lane 4",\r\n        "city": "Sarasota",\r\n        "state": "FL",\r\n        "zip": "98101"\r\n    },\r\n    "consumer": [\r\n        {\r\n            "firstName": "gokul",\r\n            "lastName": "qa",\r\n            "emailId": "gokul10@yopmail.com",\r\n            "phoneNo": "9742766425",\r\n            "country": "IN"\r\n        }\r\n    ]\r\n}'], 
    FailureHandling.STOP_ON_FAILURE)

//adding to do task

Mobile.verifyElementExist(findTestObject('C2SC_AddToDoItemScreen/btn_plusIcon'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/btn_plusIcon'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_AddToDoItemScreen/txt_AddItemToYourList'), GlobalVariable.intWaitTime)

//int randNum = ((int) (Math.random() * 1000));
Mobile.setText(findTestObject('C2SC_AddToDoItemScreen/input_Title'), 'ToDoTest', GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_AddToDoItemScreen/input_Description'), 'Description', GlobalVariable.intWaitTime)

String AndroidDevice = GlobalVariable.sDeviceAndroid

if (AndroidDevice.equals('Android')) {
    Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/input_DueDate'), GlobalVariable.intWaitTime)

    Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/icon_CalendarArrow'), GlobalVariable.intWaitTime)

    Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/txt_CalendarDate'), GlobalVariable.intWaitTime)

    Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/btn_Calendar_OK'), GlobalVariable.intWaitTime)
} else {
    Mobile.tap(findTestObject('Object Repository/C2SC_AddTeamMemberScreen/btn_Done'), GlobalVariable.intWaitTime)

    Mobile.tap(findTestObject('Object Repository/C2SC_AddToDoItemScreen/Img_CalendarIcon'), GlobalVariable.intWaitTime)

    Mobile.swipe(170, 640, 170, 525)

    Mobile.swipe(170, 640, 170, 525)

    Mobile.swipe(170, 640, 170, 490)

    Mobile.swipe(170, 640, 170, 490)

    Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/txt_CalendarDate'), GlobalVariable.intWaitTime)
}

Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/btn_Save'), GlobalVariable.intWaitTime)

Mobile.delay(GlobalVariable.intWaitTime, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/tab_ToDo'), GlobalVariable.intWaitTime)

Mobile.delay(10, FailureHandling.STOP_ON_FAILURE)

if (AndroidDevice.equals('Android')) {

Mobile.scrollToText('ToDoTest')}

else {
	    Mobile.swipe(170, 640, 170, 525)
	
		Mobile.swipe(170, 640, 170, 525)
	
		Mobile.swipe(170, 640, 170, 490)
	
		Mobile.swipe(170, 640, 170, 490)
}

Mobile.waitForElementPresent(findTestObject('C2SC_AddToDoItemScreen/txt_todoTitle'), GlobalVariable.intWaitTime)

Mobile.getText(findTestObject('C2SC_AddToDoItemScreen/txt_todoTitle'), GlobalVariable.intWaitTime)

//adding team member

Mobile.tap(findTestObject('C2SC_MyProfilePage/tab_MyTeam'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('Object Repository/C2SC_MyTeamPage/btn_AddTeamPlusIcon'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_MyTeamPage/btn_AddTeamPlusIcon'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_AddTeamMemberScreen/txt_AddTeamMemberHeader'), GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('C2SC_AddTeamMemberScreen/input_phone'), 5, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_AddTeamMemberScreen/input_phone'), '1234567890', GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('Object Repository/C2SC_MyTeamPage/input_ContactName'), 5, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('Object Repository/C2SC_MyTeamPage/input_ContactName'), 'John', GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('C2SC_AddTeamMemberScreen/input_email'), 5, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_AddTeamMemberScreen/input_email'), 'test@test.com', GlobalVariable.intWaitTime)

if (Mobile.verifyElementExist(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)) {
	Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
}

Mobile.tap(findTestObject('C2SC_AddTeamMemberScreen/icon_PlusIconTeamMember'), GlobalVariable.intWaitTime)

//add feedback

Mobile.delay(5)

Mobile.tap(findTestObject('C2SC_MyProfilePage/tab_MyProfile'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_FeedbackScreen/txt_Feedback'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_FeedbackScreen/txt_Feedback'), GlobalVariable.intWaitTime)

Mobile.verifyElementText(findTestObject('Object Repository/C2SC_FeedbackScreen/txt_FeedbackHeader'), 'Feedback')

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_FeedbackScreen/txt_FeedbackSubtitle'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_FeedbackScreen/txt_Description'), GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('Object Repository/C2SC_FeedbackScreen/Input_Description'), 'This is test description', GlobalVariable.intWaitTime)

if(Mobile.verifyElementExist(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)) {
	Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
}

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_FeedbackScreen/Btn_Submit'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_FeedbackScreen/Btn_Submit'), GlobalVariable.intWaitTime)

